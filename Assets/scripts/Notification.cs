﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Runtime.InteropServices;


public class Notification : MonoBehaviour {
	public GameObject PanelManager;
	public GameObject Text;
	public Image NotiColor;
	private string lastText = "";
	private bool lastColor = false;
	Color green = new Color32(85, 201, 69, 255);
	Color red = new Color32(201, 69, 69, 255);

	private bool isShowing = false;

	public void Show(bool Show, bool Green = default(bool), string Text = default(string))
	{
		//Set Value:
		if (Show == false) {
			Text = lastText;
			Green = lastColor;
		} else {
			lastText = Text;
			lastColor = Green;
		}
		
		this.Text.GetComponent<Text> ().text = Text;
		this.NotiColor.color = Green ? green : red;

		PanelManager.GetComponent<PanelManager>().ShowNotification(Show);
	}
}

﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class InitializeProfile : MonoBehaviour {
	public Animator ProfileWindow;
    public Sprite GuestSprite;

	//This is current Profile viewing, NOT logged in Profile!
	public string[] viewingProfile;
	private int viewingIndex;
	public Texture2D viewingProfileImage;


	public GameObject Notification;

	[System.Serializable]
	public class Profiles
	{
		public string Name;
		[Space(10)]
		public GameObject Login;
		public GameObject LogOut;
		public GameObject Username;
		public GameObject UsernameImage;
	}
	public List<Profiles> profiles = new List<Profiles>();

	[System.Serializable]
	public class Profile
	{
		public GameObject EditProfileBTN;
		public GameObject Login;
		public GameObject LogOut;
		public GameObject Username;
		public GameObject UsernameImage;
		public GameObject Icon;

		public GameObject Name;
		public GameObject Position;
		public GameObject FDD;
		public GameObject SWDuties;
		public GameObject Email;
		public GameObject Extension;
		public GameObject Room;
		public GameObject Status;
	}
	public Profile profile = new Profile ();

	[System.Serializable]
	public class EditProfile
	{
		public GameObject MenuManager;
		public Animator anim;
		public GameObject UsernameImage;

		public GameObject Name;
		public GameObject Username;
		public GameObject Position;
		public GameObject FDD;
		public GameObject SWDuties;
		public GameObject Email;
		public GameObject Extension;
		public GameObject Room;

		public GameObject Done;
		public GameObject DisabledDone;
	}
	public EditProfile editProfile = new EditProfile ();


	//Once Profile page, Home Page is click on:
	public void Initialize() {
        //Set up Profiles>>
		foreach (Profiles profile in profiles)
        {
            //Check if Logged in or not and set Login/LogOut buttons accordingly.
            //Make sure the Username is Set.
			if (GetComponent<Login> ().GetLogged ()) {
				
				if (Database.database.CurrentProfile [5] != "") {
					profile.Login.gameObject.SetActive (false);
					profile.LogOut.gameObject.SetActive (true);

					//Set Substitute Username:
					profile.UsernameImage.GetComponent<Image> ().sprite = Sprite.Create (GetComponent<Login> ().ImageRecord, new Rect (0, 0, GetComponent<Login> ().ImageRecord.width, GetComponent<Login> ().ImageRecord.height), new Vector2 (0.5f, 0.5f));  //No, then set as Substitute?
				
					profile.Username.GetComponent<Text> ().text = Database.database.CurrentProfile [1];
				}
			} else {
				profile.Login.gameObject.SetActive (true);
				profile.LogOut.gameObject.SetActive (false);

				profile.UsernameImage.GetComponent<Image> ().sprite = GuestSprite;
				profile.Username.GetComponent<Text> ().text = "Substitute";
			}

        }
	}

	//Profile Page
	public void InitializeProfilePage()
	{
		if (viewingProfile != null) {
			//Set Values of current profile:
			profile.Username.GetComponent<Text> ().text = viewingProfile [5];
			profile.Name.GetComponent<Text> ().text = viewingProfile[1];
			profile.Position.GetComponent<Text> ().text = viewingProfile [2];
			profile.FDD.GetComponent<Text> ().text = viewingProfile [3];
			profile.SWDuties.GetComponent<Text> ().text = viewingProfile [4];
			profile.Email.GetComponent<Text> ().text = viewingProfile [7];
			profile.Extension.GetComponent<Text> ().text = viewingProfile [8];
			profile.Room.GetComponent<Text> ().text = viewingProfile [9];
			switch (viewingProfile[11]) {
			case "0":
				profile.Status.GetComponent<Image> ().color = Color.gray;
				break;
			case "1":
				profile.Status.GetComponent<Image> ().color = Color.green;
				break;
			case "2":
				profile.Status.GetComponent<Image> ().color = Color.red;
				break;
			}


		if (GetComponent<Login> ().GetLogged ())
		{
			//Set Items Visible:

			//Set Admin Icon Visible if viewing an admin account
				if (viewingProfile[10] == "1")
				profile.Icon.SetActive (true);
			else
				profile.Icon.SetActive (false);

			//Is this my Profile?  OR is this an ADMIN ACCCOUNT?
				if (viewingProfile[0] == Database.database.CurrentProfile[0]) {		//Viewing Own Account
				profile.LogOut.gameObject.SetActive (true);
				profile.EditProfileBTN.SetActive (true);
			} else if (Database.database.CurrentProfile [10] == "1") {			//Viewing Other Account, While Admin
				profile.LogOut.gameObject.SetActive (false);
				profile.EditProfileBTN.SetActive (true);
			} else {														//Viewing Other Account
				profile.LogOut.gameObject.SetActive (false);
				profile.EditProfileBTN.SetActive (false);
			}
			profile.Login.gameObject.SetActive (false);

				if (viewingProfile [1] == "Administrator") 
				profile.UsernameImage.GetComponent<Image> ().sprite = Sprite.Create (GetComponent<Login>().AdminImage, new Rect (0, 0, GetComponent<Login>().AdminImage.width, GetComponent<Login>().AdminImage.height), new Vector2 (0.5f, 0.5f));
			else
					profile.UsernameImage.GetComponent<Image> ().sprite = Sprite.Create (viewingProfileImage, new Rect (0, 0, viewingProfileImage.width, viewingProfileImage.height), new Vector2 (0.5f, 0.5f));
		} else {		//Sub, no matter what!
			profile.Login.gameObject.SetActive (true);
			profile.LogOut.gameObject.SetActive (false);
			profile.EditProfileBTN.SetActive (false);
			profile.Icon.SetActive (false);

			profile.UsernameImage.GetComponent<Image> ().sprite = GuestSprite;
		}
	}
	}

	//Init Edit Profile
	public void InitializeEditProfilePage()
	{
		//User should already be logged in and set, so no need to authorize

		//Set Input Values:
		editProfile.Username.GetComponent<InputField> ().text = viewingProfile [5];
		editProfile.Name.GetComponent<InputField> ().text = viewingProfile[1];
		editProfile.Position.GetComponent<InputField> ().text = viewingProfile [2];
		editProfile.FDD.GetComponent<InputField> ().text = viewingProfile [3];
		editProfile.SWDuties.GetComponent<InputField> ().text = viewingProfile [4];
		editProfile.Email.GetComponent<InputField> ().text = viewingProfile [7];
		editProfile.Extension.GetComponent<InputField> ().text = viewingProfile [8];
		editProfile.Room.GetComponent<InputField> ().text = viewingProfile [9];

		editProfile.UsernameImage.GetComponent<Image> ().sprite = Sprite.Create (viewingProfileImage, new Rect (0, 0, viewingProfileImage.width, viewingProfileImage.height), new Vector2 (0.5f, 0.5f));

		//Admin Checks -- (Set editable On / Off)
		Color faddedColor = new Color(178/255f, 178/255f, 178/255f);
	
		if (Database.database.CurrentProfile [10] == "0") {
			editProfile.Position.transform.FindChild ("Text").GetComponent<Text> ().color = faddedColor;
			editProfile.FDD.transform.FindChild("Text").GetComponent<Text>().color = faddedColor;
			editProfile.SWDuties.transform.FindChild("Text").GetComponent<Text>().color = faddedColor;
			editProfile.Extension.transform.FindChild("Text").GetComponent<Text>().color = faddedColor;
			editProfile.Room.transform.FindChild("Text").GetComponent<Text>().color = faddedColor;

			editProfile.Position.GetComponent<InputField> ().interactable = false;
			editProfile.FDD.GetComponent<InputField> ().interactable = false;
			editProfile.SWDuties.GetComponent<InputField> ().interactable = false;
			editProfile.Extension.GetComponent<InputField> ().interactable = false;
			editProfile.Room.GetComponent<InputField> ().interactable = false;
		}
		else {
			editProfile.Position.transform.FindChild ("Text").GetComponent<Text> ().color = Color.black;
			editProfile.FDD.transform.FindChild("Text").GetComponent<Text>().color = Color.black;
			editProfile.SWDuties.transform.FindChild("Text").GetComponent<Text>().color = Color.black;
			editProfile.Extension.transform.FindChild("Text").GetComponent<Text>().color = Color.black;
			editProfile.Room.transform.FindChild("Text").GetComponent<Text>().color = Color.black;

			editProfile.Position.GetComponent<InputField> ().interactable = true;
			editProfile.FDD.GetComponent<InputField> ().interactable = true;
			editProfile.SWDuties.GetComponent<InputField> ().interactable = true;
			editProfile.Extension.GetComponent<InputField> ().interactable = true;
			editProfile.Room.GetComponent<InputField> ().interactable = true;
		}
	}

	//Update Profile
	public void OnClick_Done() {
		//error checkings:
		if (editProfile.Name.GetComponent<InputField> ().text.Length < 3)
			Notification.GetComponent<Notification> ().Show (true, false, "Name must be at least 3 character.");
		else if (editProfile.Username.GetComponent<InputField> ().text.Length < 3)
			Notification.GetComponent<Notification> ().Show (true, false, "Username must be at least 3 character.");
		else if (!CheckUserAvaliable(editProfile.Username.GetComponent<InputField> ().text))
			Notification.GetComponent<Notification> ().Show (true, false, "Sorry. User name is unavalible.");
		else if (!editProfile.Email.GetComponent<InputField> ().text.Contains("@"))
			Notification.GetComponent<Notification> ().Show (true, false, "Invaild Email Address");
		else if (Database.database.CurrentProfile [10] == "1" && editProfile.Room.GetComponent<InputField> ().text.Length < 3)
			Notification.GetComponent<Notification> ().Show (true, false, "Room Number is to be 3 character.");
		else if (Database.database.CurrentProfile [10] == "1" && editProfile.Extension.GetComponent<InputField> ().text.Length < 3)
			Notification.GetComponent<Notification> ().Show (true, false, "Phone Extension is to be 3 character.");
		


		//Everythings good:
		else {
			StartCoroutine (Database.database.UpdateProfile(editProfile.Name.GetComponent<InputField> ().text, editProfile.Username.GetComponent<InputField> ().text, editProfile.Email.GetComponent<InputField> ().text, editProfile.Position.GetComponent<InputField> ().text, editProfile.FDD.GetComponent<InputField> ().text, editProfile.SWDuties.GetComponent<InputField> ().text, editProfile.Extension.GetComponent<InputField> ().text, editProfile.Room.GetComponent<InputField> ().text, viewingProfile[0]));
			Notification.GetComponent<Notification> ().Show (true, true, "Your Information has been updated.");
			editProfile.MenuManager.GetComponent<PanelManager> ().OpenPanel (editProfile.anim);
		}
	}

	bool CheckUserAvaliable(string user) {
		foreach (string[] str in Database.database.Staff) {
			if (user == str [5] && user != viewingProfile[5])
				return false;
		}
		return true;
	}
	public void DoneBtn(bool b) {
		editProfile.DisabledDone.SetActive (!b);
		editProfile.Done.SetActive (b);
	}



	//View *Other Profile
	public void OnClick_ViewProfile(int index) {
		viewingIndex = index;
		Debug.Log (viewingIndex);
		//Users Profile:
		if (viewingIndex == -1) {
			viewingProfile = Database.database.CurrentProfile;
			viewingProfileImage = GetComponent<Login> ().ImageRecord;
		} else {
			viewingProfile = Database.database.Staff [viewingIndex];
			viewingProfileImage = Database.database.StaffImages [viewingIndex];
		}
	}

	public void OnClick_ViewOwnProfile() {
		OnClick_ViewProfile (-1);
		InitializeProfilePage ();
	}

	//View *Users Profile
	public void Update_EditProfile() {
		OnClick_ViewProfile (viewingIndex);
		InitializeEditProfilePage ();
	}


	public void OnClick_ChangeProfileImage() {
		Dialog dialog;
		dialog = ScriptableObject.CreateInstance<Dialog>() as Dialog;

		dialog.init ("Standby", "We'll be adding feature soon!", Dialog.ButtonType.OKAY);

		//Only for windows
		System.Diagnostics.Process p = new System.Diagnostics.Process();
		p.StartInfo = new System.Diagnostics.ProcessStartInfo("explorer.exe");
		p.Start();
	}
}
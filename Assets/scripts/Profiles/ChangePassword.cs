﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ChangePassword : MonoBehaviour {
	public GameObject MenuManager;
	public Animator anim;

	public GameObject DoneBTN;
	public GameObject DoneBTNDisabled;


	public GameObject Notification;
	public GameObject CurrentPassword;
	public GameObject NewPassword;
	public GameObject ConfirmPassword;

	public Sprite LockNormal;
	public Sprite LockRed;
	public Image NewLockIcon;
	public Image ConfirmLockIcon;

	bool EnabledDone = false;

	private string text = "";

	public void OnEndEdit_New() {
		if (NewPassword.GetComponent<InputField> ().text.Length < 6) {
			Notification.GetComponent<Notification> ().Show (true, false, "Passwords must be at least 6 characters.");
			NewLockIcon.sprite = LockRed;
		} else if (NewPassword.GetComponent<InputField> ().text != ConfirmPassword.GetComponent<InputField> ().text && ConfirmPassword.GetComponent<InputField> ().text != "") { 
			Notification.GetComponent<Notification> ().Show (true, false, "Passwords do not match.");
			NewLockIcon.sprite = LockRed;
			ConfirmLockIcon.sprite = LockRed;

		}
		else if (ConfirmPassword.GetComponent<InputField> ().text == "")
		{
			NewLockIcon.sprite = LockNormal;
			ConfirmLockIcon.sprite = LockNormal;
		} else {
			Notification.GetComponent<Notification> ().Show (false);
			NewLockIcon.sprite = LockNormal;
			ConfirmLockIcon.sprite = LockNormal;
		}
			
	}
	public void OnEndEdit_Confirmation() {
		//Doesn't Match:
		if (NewPassword.GetComponent<InputField> ().text != ConfirmPassword.GetComponent<InputField> ().text && ConfirmPassword.GetComponent<InputField> ().text != "" && NewPassword.GetComponent<InputField> ().text != "") { 
			Notification.GetComponent<Notification> ().Show (true, false, "Passwords do not match.");
			NewLockIcon.sprite = LockRed;
			ConfirmLockIcon.sprite = LockRed;
		} else if (ConfirmPassword.GetComponent<InputField> ().text == "") {
			ConfirmLockIcon.sprite = LockNormal;
			if (NewPassword.GetComponent<InputField> ().text.Length < 6)
				NewLockIcon.sprite = LockRed;
			else
				NewLockIcon.sprite = LockNormal;
				
		} else if (NewPassword.GetComponent<InputField> ().text == "") {
			NewLockIcon.sprite = LockRed;
			ConfirmLockIcon.sprite = LockNormal;
		}else if (NewPassword.GetComponent<InputField> ().text.Length < 6) {
			NewLockIcon.sprite = LockRed;
			ConfirmLockIcon.sprite = LockNormal;
		}
		else if (NewPassword.GetComponent<InputField> ().text == ConfirmPassword.GetComponent<InputField> ().text) {
			NewLockIcon.sprite = LockNormal;
			ConfirmLockIcon.sprite = LockNormal;
		}

	}

	public void OnValueChange() {
		if (Database.database.CurrentProfile [6] != "") {
			if (CurrentPassword.GetComponent<InputField> ().text != "" && NewPassword.GetComponent<InputField> ().text != "" && ConfirmPassword.GetComponent<InputField> ().text != "") {
				DoneBtn (true);
			} else
				DoneBtn (false);
		} else {	//allow blank password:
			if (NewPassword.GetComponent<InputField> ().text != "" && ConfirmPassword.GetComponent<InputField> ().text != "") {
				DoneBtn (true);
			} else
				DoneBtn (false);
		}

	}

	//Change Password
	public void OnClick_Done() {
		if (NewPassword.GetComponent<InputField> ().text.Length < 6)
			Notification.GetComponent<Notification> ().Show (true, false, "Passwords must be at least 6 characters.");
		else if (ConfirmPassword.GetComponent<InputField> ().text != NewPassword.GetComponent<InputField> ().text)
			Notification.GetComponent<Notification> ().Show (true, false, "Passwords do not match.");
		
		else if (CurrentPassword.GetComponent<InputField> ().text == Database.database.CurrentProfile [6]) {
			///Updates Password:
			StartCoroutine (Database.database.UpdatePassword(NewPassword.GetComponent<InputField> ().text, Database.database.CurrentProfile [0]));

			Notification.GetComponent<Notification> ().Show (true, true, "Your password has been changed.");
			MenuManager.GetComponent<PanelManager> ().OpenPanel (anim);
			//Reset:
			CurrentPassword.GetComponent<InputField> ().text = "";
			NewPassword.GetComponent<InputField> ().text = "";
			ConfirmPassword.GetComponent<InputField> ().text = "";
		
		} else
			Notification.GetComponent<Notification> ().Show (true, false, "Your old password was enter incorrectly. Please enter it again.");
	}

	public void DoneBtn(bool b) {
		DoneBTNDisabled.SetActive (!b);
		DoneBTN.SetActive (b);
	}
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class StaffManager : MonoBehaviour
{
	public GameObject SpawnPrefab;
	public GameObject SpawnContainer;

	double timer = 0.0;

	void Start() {
		Time.fixedDeltaTime = 5f;
	}

	//[System.Serializable]
	public class Staff
	{
		public int Index;
		public string Name;
		public string Status;
		public Texture2D Image;
		public GameObject spawn;

		public Staff(int index, string name, Texture2D image, string status)
		{
			Index = index;
			Name = name;
			Image = image;
			Status = status;
		}
		public void Spawn(GameObject Container, GameObject Prefab) {
			spawn = (GameObject)Instantiate (Prefab, Container.transform.localPosition, Container.transform.localRotation);
			spawn.transform.SetParent (Container.transform, false);
			spawn.transform.localScale = Container.transform.localScale;

			//Design:
			spawn.name = Name;
			spawn.transform.FindChild("Background").FindChild("Name").GetComponent<Text>().text = Name;
			spawn.transform.FindChild("Background").FindChild("Image").GetComponent<Image>().sprite = Sprite.Create (Image, new Rect (0, 0, Image.width, Image.height), new Vector2 (0.5f, 0.5f));

			if (Status.Equals("1"))		//Green:
				spawn.transform.FindChild("Background").FindChild("Status").GetComponent<Image>().color = Color.green;
			else if (Status.Equals("2"))		//Red:
				spawn.transform.FindChild("Background").FindChild("Status").GetComponent<Image>().color = Color.red;
			else 					//Gray:
				spawn.transform.FindChild("Background").FindChild("Status").GetComponent<Image>().color = Color.gray;
					
		}
		public void UpdateStaff (string name, Texture2D image, string status) {
			//Design:
			spawn.name = Name;
			spawn.transform.FindChild("Background").FindChild("Name").GetComponent<Text>().text = Name;
			spawn.transform.FindChild("Background").FindChild("Image").GetComponent<Image>().sprite = Sprite.Create (Image, new Rect (0, 0, Image.width, Image.height), new Vector2 (0.5f, 0.5f));


			if (status.Equals("1"))		//Green:
				spawn.transform.FindChild("Background").FindChild("Status").GetComponent<Image>().color = Color.green;
			else if (status.Equals("2"))		//Red:
				spawn.transform.FindChild("Background").FindChild("Status").GetComponent<Image>().color = Color.red;
			else 					//Gray:
				spawn.transform.FindChild("Background").FindChild("Status").GetComponent<Image>().color = Color.gray;

		}
	}
	public static List<Staff> profiles = new List<Staff>();


	public void Initialize() {

		if (Database.database.DatabaseLoaded) {
			int count = Database.database.Staff.Count;

			//Update already created gameobjects:
			if (profiles.Count != 0) {
				int i = 0;
				foreach (Staff n in profiles) {
					string[] sArray = Database.database.Staff [i];
					n.UpdateStaff (sArray [1], Database.database.StaffImages [i], sArray [11]);

					i++;
				}
			}

			//Create new gameobjects:
			if (profiles.Count < count) {

				//Creates a new Staff object:
				for (int i = profiles.Count; i < count; i++) {
					string[] sArray = Database.database.Staff [i];

					Staff temp = new Staff (i, sArray [1], Database.database.StaffImages[i], sArray [11]);
					profiles.Add (temp);

					//Adds A Clone Prefab to World:
					profiles [i].Spawn (SpawnContainer, SpawnPrefab);
				}
			}
		}
	}

	void FixedUpdate() 
	{
		//TESTING:
		timer += Time.deltaTime;
		Debug.Log (timer);


		StartCoroutine (UpdateStatus ());
	}

	IEnumerator UpdateStatus() {
		//Every 5 seconds UpdateStatus of each profile.
		if (Database.database.DatabaseLoaded) 
		{
			Debug.Log ("Updating Status...");
			Database.database.UpdateStatus ();
			yield return new WaitUntil (() => Database.database.GetStatus() == true);

			//Apply Changes:
			if (profiles.Count != 0) {
				int i = 0;
				foreach (Staff n in profiles) {
					string[] sArray = Database.database.Staff [i];
					n.UpdateStaff (sArray [1], Database.database.StaffImages [i], sArray [11]);

					i++;
				}
			}

		}
	}
}

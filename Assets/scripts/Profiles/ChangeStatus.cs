﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ChangeStatus : MonoBehaviour {
	int Status = 0;
	//Status:  0 = Gray, 1 = Green, 2 = Red
	public IEnumerator Initialize () {

		if (Database.database.DatabaseLoaded) {
			StartCoroutine(Database.database.UpdateStatus ());		//Wait for results...
			yield return new WaitUntil (() => Database.database.GetStatus() == true);

			Status = int.Parse(Database.database.CurrentProfile [11]);
			UpdateStatus (false);
		}
	}

	public void UpdateStatus(bool updateOnline) {
		switch (Status) {
		case 0:
			gameObject.GetComponent<Image> ().color = Color.gray;
			break;
		case 1:
			gameObject.GetComponent<Image> ().color = Color.green;
			break;
		case 2:
			gameObject.GetComponent<Image> ().color = Color.red;
			break;
		}

		if (updateOnline) {
			Database.database.CurrentProfile [11] = Status.ToString ();
			StartCoroutine (Database.database.SaveStatus ());
		}
	}
	public void OnClick() {
		if (Status > 1)
			Status = 0;
		else
			Status++;
		UpdateStatus (true);
	}
}

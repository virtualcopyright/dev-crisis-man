﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Login : MonoBehaviour {
	public Texture2D ImageRecord;
	public Texture2D AdminImage;
	public Sprite GuestImage;
	public Texture2D UnknowImage;

	public InputField usernameInput;
	public InputField passwordInput;
	public Text resultLabel;

	private const string adminUser = "Admin";
	private const string adminPass = "Pass";

	public GameObject AdminSettings;

	private bool Logged;
	public bool GetLogged() {  return Logged;  }

	private void Start()
	{
		if (CheckLogin (Get_Username(), Get_Password ())) {
			Logged = true;
			GetComponent<InitializeProfile> ().Initialize ();
		} else {
			//												Set Admin Always.... IF IN BETA ;)  REMEMBER TO TURN THIS OFF!!!
			Database.database.CurrentProfile = new string[12] {"", "Substitute", "NA", "NA", "NA", "Substitute", "", "NA", "NA", "NA", "1", "0"};
			ImageRecord = GuestImage.texture;
			Logged = false;
		}			
	}
	public void GetLogin(Animator trans)
	{
		//Login:
		if (CheckLogin (usernameInput.text, passwordInput.text)) {
			Save_User (usernameInput.text, passwordInput.text);
			passwordInput.text = "";
			resultLabel.text = "";

			Logged = true;
			GetComponent<InitializeProfile> ().Initialize ();

			GameObject.Find ("MenuManager").GetComponent<PanelManager> ().OpenPanel (trans);
		} else {
			resultLabel.text = "Incorrect Information";
		}
	}

	bool CheckLogin(string iUsername, string iPassword)
	{
		int i = 0;
		//Check in Database:
		foreach (string[] sArray in Database.database.Staff) {
			string dUsername = sArray[5];
			string dPassword = sArray[6];

			if (iUsername == dUsername && iPassword == dPassword) {
				//Set Current Record:
				Database.database.CurrentProfile = sArray;

				if (Database.database.StaffImages.Count > 0) {
					//Set Image Texture:
					ImageRecord = Database.database.StaffImages [i];
				}
				else
					ImageRecord = UnknowImage;

				return true;
			}
			i++;
		}

		//Check if Admin  -  (Works for offline)
		if (iUsername == adminUser && iPassword == adminPass) {
			Database.database.CurrentProfile = new string[12] {"", "Administrator", "NA", "NA", "NA", adminUser, adminPass, "NA", "NA", "NA", "1", "0"};

			//Make Sure Admin Gets ADMIN Settings
			AdminSettings.SetActive (true);

			ImageRecord = AdminImage;
			
			return true;
		}

		//if nothing matches return false:
		return false;
	}

	public void GetLogOut(Animator trans) {
		Save_User ("", "");
		//Reset Record:
		Start ();

        GetComponent<InitializeProfile>().Initialize();
		AdminSettings.SetActive (false);
        GameObject.Find("MenuManager").GetComponent<PanelManager>().OpenPanel(trans);
	}


	//Save Current User
	public void Save_User(string username, string password) {
		PlayerPrefs.SetString ("CurrentUsername", username);
		PlayerPrefs.SetString ("CurrentPassword", password);
	}
	public string Get_Username() {  return PlayerPrefs.GetString ("CurrentUsername");  }
	public string Get_Password() {  return PlayerPrefs.GetString ("CurrentPassword");  }

	public void UpdateCurrentRecord() {
		Database.database.CurrentProfile = Database.database.Staff[int.Parse(Database.database.CurrentProfile [0])];
		GetComponent<InitializeProfile> ().Initialize ();
	}

}

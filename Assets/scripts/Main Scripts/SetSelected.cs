﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class SetSelected : MonoBehaviour {

	public void Set(GameObject Selected)
	{
		EventSystem.current.SetSelectedGameObject(Selected);
	}
}

﻿using UnityEngine;
using System.Collections;
using System.Net;
using System.IO;

public class Connection : MonoBehaviour {

	public bool CheckConnection()
	{
		string HtmlText = GetHtmlFromUri("http://google.com");
		if (HtmlText != "" || HtmlText.Contains("schema.org/WebPage"))
			//success
			return true;
		else
			//something went wrong...
			return false;
	}

	public string GetHtmlFromUri(string resource)
	{
		string html = string.Empty;
		HttpWebRequest req = (HttpWebRequest)WebRequest.Create(resource);
		try
		{
			using (HttpWebResponse resp = (HttpWebResponse)req.GetResponse())
			{
				if ((int)resp.StatusCode < 299 && (int)resp.StatusCode >= 200) {
					using (StreamReader reader = new StreamReader(resp.GetResponseStream()))  {
						char[] cs = new char[80];
						reader.Read(cs, 0, cs.Length);
						foreach(char ch in cs)
							html +=ch;
					}
				}
			}
		}
		catch
		{
			return "";
		}
		return html;
	}
}

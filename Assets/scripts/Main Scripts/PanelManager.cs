using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;

public class PanelManager : MonoBehaviour
{
	public GameObject Status;
	public GameObject StaffManager;
	public GameObject ProfileManager;
    public GameObject MenuFunctions;
    public GameObject Theme_Tab;
	public GameObject Background;

	public GameObject EmergencyNumbers;

	//public GameObject Database;
	public GameObject QS_EditAdmin;
	public GameObject AdminSettings;

    //Animation Fields >>
    public Animator initiallyOpen;
    public Animator MenuPanel;
	public Animator Notification;


    private int OpenParameterId;
    private int ShiftParameterId;

	private int ShowParameterId;

	private Animator CurrentlyOpen;


    const string OpenStateName = "Open";
    const string ClosedStateName = "Closed";
    const string ShiftStateName = "Shift";
    const string SwitchStateName = "Switch";

	const string ShowStateName = "Show";


    //Size Up Fields >>
    //public RectTransform Menu;
    //public RectTransform MenuWindow;
    
    public void OnEnable()
    {
        OpenParameterId = Animator.StringToHash(OpenStateName);
        ShiftParameterId = Animator.StringToHash(ShiftStateName);
        //SwitchParameterId = Animator.StringToHash(SwitchStateName);
		ShowParameterId = Animator.StringToHash(ShowStateName);


        if (initiallyOpen == null)
            return;

        OpenPanel(initiallyOpen);
    }

    public void OpenPanel(Animator anim)
    {
		if (CurrentlyOpen == anim)
			return;		//Well...
		anim.gameObject.SetActive (true);

		anim.transform.SetAsLastSibling ();

		CloseCurrent ();


		CurrentlyOpen = anim;
		CurrentlyOpen.SetBool (OpenParameterId, true);

		//Set Edit Settings
		QS_EditAdmin.SetActive (false);

		//AdminSettings.SetActive (false);


		//Intitialize Window:		Updates each Page:
		if (anim.name == "Status") {
			StartCoroutine(Status.GetComponent<ChangeStatus> ().Initialize ());
		}
		if (anim.name == "Profile") {
			ProfileManager.GetComponent<InitializeProfile> ().InitializeProfilePage ();
			Debug.Log (Database.database.Staff [2] [11]);
		}
		if (anim.name == "Profile Settings") {
			ProfileManager.GetComponent<InitializeProfile> ().Update_EditProfile ();
		}
		if (anim.name == "Staff") {
			StaffManager.GetComponent<StaffManager> ().Initialize ();
		}
		if (anim.name == "Home") {
			ProfileManager.GetComponent<InitializeProfile> ().Initialize ();
			EmergencyNumbers.GetComponent<TelephoneSorter> ().Initialize ();
			Theme_Tab.GetComponent<ThemeManager> ().Intialize ();
			Background.GetComponent<BackgroundManager> ().Intialize ();

		} else if (anim.name == "Procedures" || anim.name == "Menu") {
			//Initializes Names every time you open the window ... from(Offline Database):
			anim.gameObject.transform.FindChild ("Window").GetComponent<ProcedureTabs> ().Initialize ();


			//Set Quick Menu Edit button active if Admin
			if (Database.database.CurrentProfile [10] == "1")
				QS_EditAdmin.SetActive (true);	
						
		}
	

        //Loop through Menu Buttons until you find the name of the Panel Opening
        //Then set that GameObject Selected
        bool found = false;
        for (int i = 0; i < MenuFunctions.GetComponent<MenuFunctions>().menuButtons.Length; i++)
        {
            if (MenuFunctions.GetComponent<MenuFunctions>().menuButtons[i].Name == anim.name)
            {
                MenuFunctions.GetComponent<MenuFunctions>().SelectedManager(MenuFunctions.GetComponent<MenuFunctions>().menuButtons[i].GameObject);
                found = true;
                break;
            }
        }
        if (!found) {
            //Set Menu Button NOT selected:
            MenuFunctions.GetComponent<MenuFunctions>().SelectedManager(GameObject.Find("EmptyObject - Selected"));
        }
    }




    public void ShiftPanel(bool isMenuOpen)
    {
        //this.isMenuOpen = isMenuOpen;

        if (CurrentlyOpen == null)
            return;

        //Transition Menu Panel and Menu Bar:
            MenuPanel.SetBool(ShiftParameterId, isMenuOpen ? true : false);
        //Transition Current Panel Open at the time:
            CurrentlyOpen.SetBool(ShiftParameterId, isMenuOpen ? true : false);

			StartCoroutine(ShiftComplete(MenuPanel));
    }

	public void ShowNotification(bool isShowing)
	{
		//Transition Menu Panel and Menu Bar:
		Notification.SetBool(ShowParameterId, isShowing ? true : false);

		if (isShowing)
			StartCoroutine(WaitForFinishedNotif());
	}
	IEnumerator WaitForFinishedNotif ()
	{
		yield return new WaitForSeconds (3);
		Notification.GetComponent<Notification> ().Show (false);
	}


    public void CloseCurrent()
    {
        if (CurrentlyOpen == null)
            return;

        ////Instead Close using Switch Anim
        //if (isMenuOpen)
        //{
        //    CurrentlyOpen.SetBool(SwitchParameterId, true);
        //    CurrentlyOpen = null;
        //}
        //else
        //{
            CurrentlyOpen.SetBool(OpenParameterId, false);
            StartCoroutine(DisablePanelDeleyed(CurrentlyOpen));
            CurrentlyOpen = null;

      //  }
    }

    IEnumerator DisablePanelDeleyed(Animator anim)
    {
        bool closedStateReached = false;
        bool wantToClose = true;
        while (!closedStateReached && wantToClose)
        {
            if (!anim.IsInTransition(0))
                closedStateReached = anim.GetCurrentAnimatorStateInfo(0).IsName(ClosedStateName);

            wantToClose = !anim.GetBool(OpenParameterId);

            yield return new WaitForEndOfFrame();
        }

        if (wantToClose)
        {
            anim.gameObject.SetActive(false);

        }
    }

	IEnumerator ShiftComplete(Animator anim)
	{
		bool shiftStateReached = false;
		bool wantToShift = true;
		while (!shiftStateReached && wantToShift)
		{
			if (!anim.IsInTransition(0))
				shiftStateReached = anim.GetCurrentAnimatorStateInfo(0).IsName(SwitchStateName);
			
			wantToShift = !anim.GetBool(ShiftParameterId);
			
			yield return new WaitForEndOfFrame();
		}
		
		if (wantToShift)
		{
			Vector2 temp = MenuFunctions.transform.FindChild("Menu Bar").FindChild("Menu Button").GetComponent<RectTransform>().sizeDelta;
			temp.y = 1200;
            MenuFunctions.transform.FindChild("Menu Bar").FindChild("Menu Button").GetComponent<RectTransform>().sizeDelta = temp;
		}
	}

    public void SetCurrentlyOpen(Animator anim)
    {
        CurrentlyOpen = anim;
    }

	//When enabled... (GO to Open panel to find enabling button.)
	public void OnEditClick(GameObject text)
	{
		if (text.GetComponent<Text>().text == "Edit (Admin)")
			text.GetComponent<Text>().text = "Done";
		else
			text.GetComponent<Text>().text = "Edit (Admin)";

		//When Avaliable:
		if (CurrentlyOpen.name == "Procedures")
			CurrentlyOpen.gameObject.transform.FindChild("Window").GetComponent<ProcedureTabs>().OnEditClick();
	}

	public void setNotificationMode(bool boolean) {
		if (boolean) {
			CurrentlyOpen.gameObject.GetComponent<GraphicRaycaster> ().enabled = false;
			MenuFunctions.transform.FindChild ("Menu Bar").GetComponent<GraphicRaycaster> ().enabled = false;
		} else {
			CurrentlyOpen.gameObject.GetComponent<GraphicRaycaster> ().enabled = true;
			MenuFunctions.transform.FindChild ("Menu Bar").GetComponent<GraphicRaycaster> ().enabled = true;
		}
	}
}
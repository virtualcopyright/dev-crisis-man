using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;
    
public class ProcedureTabs : MonoBehaviour
{
	public GameObject QuickMenu_Edit;
    private GameObject CurrentlySelected_Tab;
	private GameObject CurrentlySelected_OpenTab;

    [System.Serializable]
    public class Tabs
    {
        public string Name;
        [Space(10)]
        public GameObject TAB;
        public GameObject OpenTab;
		public GameObject InputTab;
    }
    public Tabs[] tabs;


    // Once Page is Opened
	public void Initialize()
    {
		//Sets The names of the tabs
		if (Database.database.DatabaseLoaded) {
			int i = 0;
			foreach (Tabs t in tabs) {
				//Sets each Tab Input Field name to the coordinating Database Procedure Name:
				t.TAB.transform.FindChild ("InputField").GetComponent<InputField> ().text = Database.database.Procedures [i] [1];

				//Gets Saved Background:
				t.OpenTab.GetComponent<Text> ().fontSize = PlayerPrefs.GetInt("fontSize", 16);

				i++;
			}
		}
    }

    public void OnClick(GameObject _tab)
    {
		//Set Selected Button:
		CurrentlySelected_Tab = _tab;

		GameObject openTab = new GameObject();
		openTab.hideFlags = HideFlags.HideInHierarchy;

        //Grap Text from tabs
        foreach (Tabs t in tabs)
        {
            if (t.TAB.name == _tab.name)
            {
				//Set Selected Tab:
				openTab = t.OpenTab;
                break;
            }
        }


		openTab.SetActive(!openTab.activeSelf);

		if (CurrentlySelected_OpenTab == null)
			CurrentlySelected_OpenTab = openTab;
		else if (openTab != CurrentlySelected_OpenTab)
        {
			CurrentlySelected_OpenTab.SetActive(false);
			CurrentlySelected_OpenTab = openTab;
        }

        LoadText();
    }

    void LoadText()
    {
        //Check in Database:
		foreach (string[] sArray in Database.database.Procedures)
        {
            string Name = sArray[1];
            string Text = sArray[2];

            if (Name == CurrentlySelected_Tab.transform.FindChild("InputField").FindChild("name").GetComponent<Text>().text)
            {
				CurrentlySelected_OpenTab.GetComponent<Text>().text = Text;

                return;
            }
        }
    }


	public void OnEditClick()
	{
		if (CurrentlySelected_OpenTab != null)
			CurrentlySelected_OpenTab.SetActive (false);

		//Sets Name and Text Input Fields On:
		//Updates Database:
		int i = 0;

		List<string[]> UpdateProcedures = new List<string[]> ();
		bool flag = false;

		foreach (Tabs t in tabs) {
			//Edit ON:
			if (t.TAB.transform.FindChild ("InputField").GetComponent<InputField> ().enabled == false) {
				//Name:
				t.TAB.transform.FindChild ("InputField").GetComponent<InputField> ().enabled = true;

				//Text:
				t.InputTab.SetActive (true);
				t.InputTab.GetComponent<InputField> ().text = Database.database.Procedures [i] [2];
                
				//Add padding of 20 to content when editing (so that you can scroll)
				if (i == 0) {
					RectOffset padding = gameObject.transform.FindChild ("Scroll View").FindChild ("Content").GetComponent<VerticalLayoutGroup> ().padding;
					padding.right = 40;
					gameObject.transform.FindChild ("Scroll View").FindChild ("Content").GetComponent<VerticalLayoutGroup> ().padding = padding;
				}
			} else {
				flag = true;

				t.TAB.transform.FindChild ("InputField").GetComponent<InputField> ().enabled = false;

				t.InputTab.SetActive (false);
			
				//Sets padding back to default
				if (i == 0) {
					RectOffset padding = gameObject.transform.FindChild ("Scroll View").FindChild ("Content").GetComponent<VerticalLayoutGroup> ().padding;
					padding.right = 20;
					gameObject.transform.FindChild ("Scroll View").FindChild ("Content").GetComponent<VerticalLayoutGroup> ().padding = padding;
				}

				//Gather data:
				string Name = t.TAB.transform.FindChild ("InputField").FindChild ("name").GetComponent<Text> ().text;
				string Text = t.InputTab.transform.FindChild ("Text").GetComponent<Text> ().text;
				string ID = Database.database.Procedures [i] [0];
				UpdateProcedures.Add (new string[] { ID, Name, Text });
			}

			i++;
		}
		if (flag)
		//Done,  Applying Changes:
		StartCoroutine (Database.database.UpdateProcedures (UpdateProcedures));

	}
}

	
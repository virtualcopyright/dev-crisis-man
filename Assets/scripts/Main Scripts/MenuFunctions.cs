﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SocialPlatforms;

public class MenuFunctions : MonoBehaviour {
	public GameObject MenuButton;

    //Panel Menu:
   // public RectTransform Menu;
    public GameObject HomeColor;
	private bool isMenuOpen = false;

	public GameObject PanelManager;

	//Quick Menu:
	public RectTransform quickMenuContainer;
	public RectTransform quickMenuButton;
	public bool isQuickOpen;
	private float btnXPos;
	
	private bool onStartAnim_Done = false;
	
	//Quick Menu Rotation:
	float currentAngleZ;

	private int countClicks = 0;
	private bool targetAt180 = true;


	//Menu Buttons:
	public Sprite SelectedMenuSprite;
	public Sprite MenuSprite;
	private GameObject SelectedMenuButton;
	public GameObject c9Text;


    //Menu Selectable Buttons      //MUST ADD New Menu Buttons HERE

    [System.Serializable]
    public class MenuButtons
    {
        [Header("Name = Panel Name  || Any if none")]
        public string Name;
        public GameObject GameObject;
    }
    public MenuButtons[] menuButtons;


	// Use this for initialization
	void Start () 
	{
		//Quick Menu:
		quickMenuContainer.gameObject.SetActive (true);
		isQuickOpen = false;

		//Set Scale, get pos of container, and set pos and get rotation of quick menu:
		quickMenuContainer.localScale = new Vector3 (0, 1, 1);
		btnXPos = quickMenuContainer.localPosition.x;
		
		quickMenuButton.localPosition = new Vector3 (btnXPos, 1000, 0);
		
		//Update Angle to 0
		currentAngleZ = quickMenuButton.eulerAngles.z;
	}
	
	// Update is called once per frame
	void Update () 
	{
	//For Quick Menu:  >>
		//Update Scale of Container:
		Vector3 scale = quickMenuContainer.localScale;
																				//  Open, Close
		scale.x = Mathf.Lerp (scale.x, isQuickOpen ? 1 : 0, Time.deltaTime * (isQuickOpen ? 8 : 14));
		if (scale.x < 0.01 && isQuickOpen == false) { 
			scale.x = 0;
		}
		quickMenuContainer.localScale = scale;
		
		
		//Update Position of Button:
		Vector3 position = quickMenuButton.localPosition;
																												//  Open, Close
		position.x = Mathf.Lerp (position.x, isQuickOpen ? (btnXPos + 120) : btnXPos, Time.deltaTime * (isQuickOpen ? 7 : 4));
		quickMenuButton.localPosition = position;
		
		//Update Rotation of Button
		currentAngleZ = Mathf.LerpAngle(currentAngleZ, isQuickOpen ? (targetAt180 ? 90 : 270) : (targetAt180 ? 180 : 0), Time.deltaTime * (isQuickOpen ? 6 : 6));
		switch (countClicks) 
		{
		case 0:
			targetAt180 = false;
			break;
		case 1:
			targetAt180 = true;
			break;
		case 2:
			targetAt180 = true;
			break;
		case 3:
			targetAt180 = false;
			break;
		}
		
		quickMenuButton.eulerAngles = new Vector3(0, 0, currentAngleZ);

		//Change Y Button Accordingly
		Vector3 position_y = quickMenuButton.FindChild ("Button").localPosition;
		position.y = isQuickOpen ? -10 : 10;
		quickMenuButton.FindChild ("Button").localPosition = position_y;


		//Update Position Y if not there:
		if (onStartAnim_Done == false) {
			QuickMenuButton_OnStart ();
		}

	}
	
	//Updates Quick Menu Start up Animation.
	void QuickMenuButton_OnStart()
    {
		Vector3 postition = quickMenuButton.localPosition;
		postition.y = Mathf.Lerp (postition.y, -10, Time.deltaTime * 8);
		quickMenuButton.localPosition = postition;
		
		if (postition.y == -10) {
			onStartAnim_Done = true;
		}
	}
	
	public void OnClick_QuickMenu()
    {
		isQuickOpen = !isQuickOpen;

		countClicks++;
		if (countClicks == 4)
			countClicks = 0;
	}

    //Closes or Opens Panel Menu   || Can also be used instead of Selected Manager to deselect and select a Menu Button :)
	public void OnClick_PanelMenu()
    {
		isMenuOpen = !isMenuOpen;

		//Set Button to full height when open:
			Vector2 temp = MenuButton.GetComponent<RectTransform> ().sizeDelta;
			temp.y = isMenuOpen ? 1200 : 80;
			MenuButton.GetComponent<RectTransform> ().sizeDelta = temp;

        //Quick Menu (Close):
		if (isQuickOpen == true) {
			isQuickOpen = false;

			countClicks++;
			if (countClicks == 4)
				countClicks = 0;
		}

		//Set c9 text
		if(isMenuOpen)
			c9Text.SetActive(true);
		else
			c9Text.SetActive (false);

        //Shift Whatever window is open at the time and Menu Bar:
        PanelManager.GetComponent<PanelManager>().ShiftPanel(isMenuOpen);
    }


//Buttons Methods: >>
	public void SelectedManager(GameObject button) {
		//Deselect Current:
		if (SelectedMenuButton != null)
			SelectedMenuButton.GetComponent<Image> ().sprite = MenuSprite;

        if (button.name != "EmptyObject - Selected")
        {
            //Select new Current:
            SelectedMenuButton = button;
            SelectedMenuButton.GetComponent<Image>().sprite = SelectedMenuSprite;
        }
	}

    public void OnClick_OpenPanel(Animator anim)
    {
        //Close Menu:
        OnClick_PanelMenu();

        //Action:
        PanelManager.GetComponent<PanelManager>().OpenPanel(anim);
    }

    public void OnClick_Central9()
    {
        OnClick_PanelMenu();

        //Action:
        Application.OpenURL("http://central9.k12.in.us/");
    }
}
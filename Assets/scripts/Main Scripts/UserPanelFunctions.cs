﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using UnityEngine.UI;

public class UserPanelFunctions : MonoBehaviour {
	
	public RectTransform container;

	private bool isOpen;
	
	// Use this for initialization
	void Start () {
		container.gameObject.SetActive (true);
		isOpen = false;
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 scale = container.localScale;
		scale.y = Mathf.Lerp (scale.y, isOpen ? 1 : 0, Time.deltaTime * 12);
		if (scale.y < 0.005 && isOpen == false) { 
			scale.y = 0;
		}
		container.localScale = scale;
	}

	public void OnClick() {
		isOpen = !isOpen;
	}
		

	public void Ondeselect() {
		StartCoroutine (Wait ());
	}

	IEnumerator Wait() {
		float waitTime = 0.1f;
		switch (PlayerPrefs.GetString ("DropboxDelay", "100 milliseconds")) {
		case "10 milliseconds":
			waitTime = 0.01f;
			break;
		case "50 milliseconds":
			waitTime = 0.05f;
			break;
		case "100 milliseconds":
			waitTime = 0.1f;
			break;
		case "250 milliseconds":
			waitTime = 0.25f;
			break;
		case "500 milliseconds":
			waitTime = 0.5f;
			break;
		}
		yield return new WaitForSeconds(waitTime);
		isOpen = false;
	}

}

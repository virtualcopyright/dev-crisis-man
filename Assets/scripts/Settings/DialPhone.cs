﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class DialPhone : MonoBehaviour, IPointerClickHandler {
	Dialog DialogBox;

	string number;

	void Start() {
		DialogBox = ScriptableObject.CreateInstance<Dialog>() as Dialog;
	}

	public void OnPointerClick(PointerEventData data) {
		foreach (TelephoneSorter.Numbers n in TelephoneSorter.numbers) {
			if (gameObject.name == n.Name || gameObject.name == "911") {
				//Creates DialogBox for Confirmation:

				if (gameObject.name == "911")
					number = "911";
				else
					number = n.Number;
				
				DialogBox.DialogExited += this.OnDialogExited;		//Subscribe to the event here.
				DialogBox.init ("CONFIRMATION", "Are you sure you would like to call: " + n.Name, Dialog.ButtonType.YESNO);

				break;
			}
				
		}
	}

	//Better Function:
	public void OnDialogExited(Dialog.ResultType result) 
	{
		if (result == Dialog.ResultType.YES)
			Application.OpenURL ("tel://" + number);
		DialogBox.DialogExited -= this.OnDialogExited;		//UnSubscribe to the event.
	}

	/*void Update() {
		if (DialogBox.Closing) {
			if (DialogBox.getResult() == Dialog.ResultType.YES)
				Application.OpenURL ("tel://" + number);
		}
	}*/
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DebugLog : ScriptableObject {
	public GameObject Text;


	public void Log (string log) {
		Text.GetComponent<Text> ().text += "\n" + log;
	}
}

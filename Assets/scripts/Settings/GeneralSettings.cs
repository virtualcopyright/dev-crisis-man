using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class GeneralSettings : MonoBehaviour {
	public GameObject LoadPhotosName;
	public GameObject AutoUpdateName;

	// Use this for initialization... More so Variables
	void Awake () {

		UpdateState();
	}


	//Functions:
	public void OnClick_AutoUpdate () {
		Database.database.AutoUpdate = !Database.database.AutoUpdate;

		//Save:
		int i = Database.database.AutoUpdate ? 1 : 0;
		if (i == 0)
			Database.database.Timer = 0;	//reset inteval timer
		PlayerPrefs.SetInt ("AutoUpdate", i);

		UpdateState();
	}

	void UpdateState () {
		if (Database.database.AutoUpdate) {
			AutoUpdateName.GetComponent<Text> ().text = "Auto Update: ON";
			Database.database.UpdateDatabase ();
		}
		else
			AutoUpdateName.GetComponent<Text>().text = "Auto Update: OFF";
	}



	//Font:
	public void Save_FontSize(GameObject text) {
		PlayerPrefs.SetInt("fontSize", int.Parse( text.GetComponent<InputField> ().text));
	}
	public void OnClick_FontSize(GameObject text) {
		text.GetComponent<InputField>().text =  PlayerPrefs.GetInt("fontSize", 16).ToString();
	}

	//Dropbox Delay
	public void Save_DropboxDelay(GameObject dropbox) {
		PlayerPrefs.SetString("DropboxDelay", dropbox.GetComponent<Dropdown>().options[dropbox.GetComponent<Dropdown>().value].text);
	}
	public void OnClick_DropboxDelaySelected(GameObject dropbox) {
		for (int i = 0; i < dropbox.GetComponent<Dropdown>().options.Count; ++i) {
			if (dropbox.GetComponent<Dropdown> ().options [i].text == PlayerPrefs.GetString ("DropboxDelay", "100 milliseconds"))
				dropbox.GetComponent<Dropdown> ().value = i;
		}
	}

	//AutoUpdate Time
	public void Save_AutoUpdateTime(GameObject dropbox) {
		PlayerPrefs.SetString("AutoUpdateTime", dropbox.GetComponent<Dropdown>().options[dropbox.GetComponent<Dropdown>().value].text);
		Database.database.AutoMinutes = ClipMinutes(PlayerPrefs.GetString("AutoUpdateTime"));
	}
	public void OnClick_UpdateAutoSelected(GameObject dropbox) {
		for (int i = 0; i < dropbox.GetComponent<Dropdown>().options.Count; ++i) {
			if (dropbox.GetComponent<Dropdown> ().options [i].text == PlayerPrefs.GetString ("AutoUpdateTime", "10 Minutes"))
				dropbox.GetComponent<Dropdown> ().value = i;
		}
	}

	int ClipMinutes (string str) {
		string refinedString = "";
		foreach (char c in str) {
			if (c != ' ')
				refinedString += c;
			else
				break;
		}
		return int.Parse(refinedString);
	}
}

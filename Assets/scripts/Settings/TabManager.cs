﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using UnityEngine.UI;

public class TabManager : MonoBehaviour
{
    private GameObject CurrentlySelected;

	[System.Serializable]
	public class Tabs
	{
		public string Name;
		[Space(10)]
		public GameObject TAB;
		public GameObject Content;
	}
	public Tabs[] tabs;


    public void OnClick(GameObject tab)
    {
		GameObject content = new GameObject();
		content.hideFlags = HideFlags.HideInHierarchy;

		//Grap Text from tabs
		foreach (Tabs t in tabs)
		{
			if (t.TAB.name == tab.name)
			{
				content = t.Content;
				break;
			}
		}

		content.SetActive(!content.activeSelf);

		if (CurrentlySelected == null)
			CurrentlySelected = content;
		else if (content != CurrentlySelected)
		{
			CurrentlySelected.SetActive(false);
			CurrentlySelected = content;
		}
    }
	

}
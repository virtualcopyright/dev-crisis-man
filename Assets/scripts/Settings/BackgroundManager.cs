﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using UnityEngine.UI;

public class BackgroundManager : MonoBehaviour
{
	
	//Settings:
	public GameObject Background;
	public GameObject Particle_System;
	public GameObject Camera;

	private Color SelectedBackground;
	

	void Start()
	{
		//Sets Saved Background   /  Converts color to Hex string:
		string setColorStr = PlayerPrefs.GetString("SelectedBackground", ColorToHex(Camera.GetComponent<Camera>().backgroundColor));
		
		GameObject setColor = new GameObject();
		setColor.AddComponent<Image>();
		Color color = new Color();
		
		//Converts Hex string to color;
		color = HexToColor(setColorStr);
		
		setColor.GetComponent<Image>().color = color;

		//Sets:
		OnClick(setColor);
	}
	
	//Remote Initialize
	public void Intialize()
	{
		Start();
	}
	
	
	public void OnClick(GameObject background)
	{
		//Change Background
		Camera.GetComponent<Camera>().backgroundColor = background.GetComponent<Image>().color;

		//Particle System?
		if (background.GetComponent<Image> ().color == gameObject.transform.FindChild ("Space_BTN").GetComponent<Image> ().color)
			Particle_System.SetActive (true);
		else
			Particle_System.SetActive (false);
		
		//Save:
		PlayerPrefs.SetString("SelectedBackground", ColorToHex(background.GetComponent<Image>().color));


	}

	
	//Color Converter:
	string ColorToHex(Color32 color)
	{
		string hex = color.r.ToString("X2") + color.g.ToString("X2") + color.b.ToString("X2");
		return hex;
	}
	
	Color HexToColor(string hex)
	{
		hex = hex.Replace("0x", "");//in case the string is formatted 0xFFFFFF
		hex = hex.Replace("#", "");//in case the string is formatted #FFFFFF
		
		byte r = byte.Parse(hex.Substring(0, 2), System.Globalization.NumberStyles.HexNumber);
		byte g = byte.Parse(hex.Substring(2, 2), System.Globalization.NumberStyles.HexNumber);
		byte b = byte.Parse(hex.Substring(4, 2), System.Globalization.NumberStyles.HexNumber);
		byte a = 200;       //Default Alpha
		
		return new Color32(r, g, b, a);
	}
}

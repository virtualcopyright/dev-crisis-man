using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using UnityEngine.UI;

public class ThemeManager : MonoBehaviour
{

    //Settings:
    public GameObject MenuMenu;
    public GameObject MenuBar;
    private Color SetlectedTheme;

    public GameObject DefaultTheme;


    // Use this for initialization
    void Start()
    {

        //Sets Saved Theme   /  Converts color to Hex string:
        string setColorStr = PlayerPrefs.GetString("SelectedTheme", ColorToHex(DefaultTheme.GetComponent<Image>().color));

        GameObject setColor  = new GameObject();
        setColor.AddComponent<Image>();
        Color color = new Color();

        //Converts Hex string to color;
        color = HexToColor(setColorStr);

        setColor.GetComponent<Image>().color = color;

        OnClick(setColor);
    }

    //Remote Initialize
    public void Intialize()
    {
        Start();
    }


    public void OnClick(GameObject Theme)
    {
        //Change Side Window
        MenuMenu.GetComponent<Image>().color = Theme.GetComponent<Image>().color;

        //Change Top Menu Bar
        MenuBar.GetComponent<Image>().color = Theme.GetComponent<Image>().color;

        //Save:
        PlayerPrefs.SetString("SelectedTheme", ColorToHex(Theme.GetComponent<Image>().color));
    }



    //Color Converter:
    string ColorToHex(Color32 color)
    {
        string hex = color.r.ToString("X2") + color.g.ToString("X2") + color.b.ToString("X2");
        return hex;
    }

    Color HexToColor(string hex)
    {
        hex = hex.Replace("0x", "");//in case the string is formatted 0xFFFFFF
        hex = hex.Replace("#", "");//in case the string is formatted #FFFFFF

        byte r = byte.Parse(hex.Substring(0, 2), System.Globalization.NumberStyles.HexNumber);
        byte g = byte.Parse(hex.Substring(2, 2), System.Globalization.NumberStyles.HexNumber);
        byte b = byte.Parse(hex.Substring(4, 2), System.Globalization.NumberStyles.HexNumber);
        byte a = 200;       //Default Alpha

        return new Color32(r, g, b, a);
    }
}

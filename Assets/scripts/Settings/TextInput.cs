﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TextInput : MonoBehaviour {
	private TouchScreenKeyboard keyboard;
	private string inputText = string.Empty;


	public void OnClick (InputField feild) {
		feild.ActivateInputField ();
	}
	public void OnTouchText (InputField feild) {
		keyboard = TouchScreenKeyboard.Open (feild.text , TouchScreenKeyboardType.Default, true, false, true, false, feild.transform.FindChild("Password Label").GetComponent<Text>().text);
	}

	void Update () {
		if(keyboard == null)
			return;
		inputText = keyboard.text;
		if(keyboard.done)
			Debug.Log("User typed in "+inputText);
		if(keyboard.wasCanceled)
			Debug.Log("User canceled input");
	}

}

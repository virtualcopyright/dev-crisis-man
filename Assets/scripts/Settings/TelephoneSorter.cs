﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class TelephoneSorter : MonoBehaviour
{
	public GameObject SpawnPrefab;
	public GameObject TelephoneContainer;

	//[System.Serializable]
	public class Numbers
	{
		public string ID;
		public string Name;
		public string  Number;
		public GameObject spawn;

		public Numbers(string id, string name, string number)
		{
			ID = id;
			Name = name;
			Number = number;
		}
		public void Spawn(GameObject Container, GameObject Prefab) {
			spawn = (GameObject)Instantiate (Prefab, Container.transform.localPosition, Container.transform.localRotation);
			spawn.transform.SetParent (Container.transform, false);
			spawn.transform.localScale = Container.transform.localScale;

			//Design:
			spawn.name = Name;
			spawn.GetComponent<Text>().text = ID + ": " + Name;
		}

	}
	public static List<Numbers> numbers = new List<Numbers>();


	public void Initialize() {
		
		if (Database.database.DatabaseLoaded) {
			int count = Database.database.EmergencyNumbers.Count;

			//Update already created gameobjects:
			if (numbers.Count != 0) {
				int i = 0;
				foreach (Numbers n in numbers) {
					string[] sArray = Database.database.EmergencyNumbers [i];
					n.Name = sArray [1];
					n.Number = sArray [2];
					//Set GameObject ... Label			**HIDE Number**
					n.spawn.GetComponent<Text> ().text = sArray [0] + ": " + sArray [1];

					i++;

				}
			}

			//Create new gameobjects:
			if (numbers.Count < count) {
				
				//Creates a new Numbers object:
				for (int i = numbers.Count; i < count; i++) {
					string[] sArray = Database.database.EmergencyNumbers [i];

					Numbers temp = new Numbers (sArray[0], sArray [1], sArray [2]);
					numbers.Add (temp);

					//Adds A Clone Prefab to World:
					numbers [i].Spawn (TelephoneContainer, SpawnPrefab);
				}
			}
		}
	}
}

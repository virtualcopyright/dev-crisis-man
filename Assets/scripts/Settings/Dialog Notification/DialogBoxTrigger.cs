﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class DialogBoxTrigger : MonoBehaviour,  IPointerClickHandler {
	public Dialog.ResultType TriggerResult;
	public void OnPointerClick(PointerEventData data) {
		gameObject.transform.parent.GetComponent<TrackDialogInstance> ().DialogInstance.OnClick (TriggerResult);
	}
}

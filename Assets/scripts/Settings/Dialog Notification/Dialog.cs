﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;


//***This Is A Non-GameObject Class***//
[CreateAssetMenu]
public class Dialog : ScriptableObject
{
	DebugLog debug;

	public string Title;
	public string  Message;
	public GameObject Prefab;

	//Button Types:
	public enum ButtonType{ OKAY, YESNO, OKAY_INTERNETCONNECTION, LOADING };
	public enum ResultType { YES, NO, NULL};

	private ResultType Result = ResultType.NULL;
	//public bool Closing = false;


	//Use a delegate to invoke a Event to tell the publisher that the button was pressed:
	public delegate void DialogExitedEventHandler(Dialog.ResultType result);
	public event DialogExitedEventHandler DialogExited;



	//For running DEBUG in application:
	void OnEnable() {
		debug = GameObject.FindObjectOfType<DebugLog>();
	}

	//Initialize
	public void init(string Title, string Message, ButtonType Type) {
		Mode (true);

		this.Title = Title;
		this.Message = Message;

		//Gets an instance of Prefab... to create gameobject
		Prefab = Instantiate(Resources.Load("Dialog/" + Type.ToString(), typeof	(GameObject)) as GameObject);

		if (Type != ButtonType.LOADING) {
			//SET		--//Make sure PATH is CORRECT
			Prefab.transform.FindChild ("Content").FindChild ("Title").GetComponent<Text> ().text = Title;
			Prefab.transform.FindChild ("Content").FindChild ("Message").GetComponent<Text> ().text = Message;
			Prefab.transform.FindChild ("Content").FindChild ("Center Button").GetComponent<TrackDialogInstance> ().DialogInstance = this;
		} else {

		}

		Prefab.transform.SetParent (GameObject.Find ("Canvas").transform, false);
		Prefab.transform.localPosition = new Vector3 (0, 0, 0);
		Prefab.transform.localScale = new Vector3 (1, 1, 1);
	}
		
	//On Click Button:
	public void OnClick(ResultType result) {
		Result = result;

		Mode (false);
		Destroy (Prefab);

		//Notify that we are done and have the results... using a delegate :)
		OnDialogExited();	//Kinda like a return function.
	}

	//Delegate Method  - Invoke Event:
	protected virtual void OnDialogExited() {
		//If there's any subscribers.
		if (DialogExited != null)
			DialogExited (Result);
	}


	//Other Methods:
	public void Mode (bool boolean) {
		//Set Notification Mode:
		Scene scene = SceneManager.GetActiveScene();
		if (scene.name == "Main")
			GameObject.FindObjectOfType<PanelManager> ().setNotificationMode (boolean);
		else
			ClickMode.canClick = boolean ? false : true;
	}

	public ResultType GetResult() {
		//Closing = false;
		Debug.Log ("Here");
		return Result;
		Debug.Log ("Not Here");
		Result = ResultType.NULL;
	}
	public bool HasResult() {
		if (Result == ResultType.NULL)
			return false;
		else
			return true;
	}
}
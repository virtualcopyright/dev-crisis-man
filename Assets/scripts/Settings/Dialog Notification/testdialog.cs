﻿using UnityEngine;
using System.Collections;

public class testdialog : MonoBehaviour {
	Dialog dialog;
	DebugLog debug;

	string number;

	void Start() {
		dialog = ScriptableObject.CreateInstance<Dialog>() as Dialog;
	}


	public void OnClick () {
		dialog.init ("CONFIRMATION", "Test Successful " + number, Dialog.ButtonType.OKAY);
	}
}

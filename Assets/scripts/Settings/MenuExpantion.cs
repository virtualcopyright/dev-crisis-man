﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SocialPlatforms;

public class MenuExpantion : MonoBehaviour {

	public RectTransform quickMenuContainer;
	public RectTransform quickMenuButton;
	public bool isOpen;

	private float btnXPos;

	private bool onStartAnim_Done = false;

	//Quick Menu Rotation:
	float currentAngleZ;
	private bool targetAt180 = true;

	// Use this for initialization
	void Start () {
		quickMenuContainer.gameObject.SetActive (true);
		isOpen = false;
		//Set Scale, get pos of container, and set pos and get rotation of quick menu:
		quickMenuContainer.localScale = new Vector3 (0, 1, 1);
		btnXPos = quickMenuContainer.localPosition.x;

		quickMenuButton.localPosition = new Vector3 (btnXPos, 1000, 0);

		//Update Angle to 0
		currentAngleZ = quickMenuButton.eulerAngles.z;
	}
	
	// Update is called once per frame
	void Update () {

		//For Quick Menu:  >>
		//Update Scale of Container
		Vector3 scale = quickMenuContainer.localScale;
																			//  Open, Close
		scale.x = Mathf.Lerp (scale.x, isOpen ? 1 : 0, Time.deltaTime * (isOpen ? 8 : 14));
		if (scale.x < 0.01 && isOpen == false) { 
			scale.x = 0;
		}
		quickMenuContainer.localScale = scale;


		//Update Position of Button
		Vector3 postition = quickMenuButton.localPosition;
																									//  Open, Close
		postition.x = Mathf.Lerp (postition.x, isOpen ? (btnXPos + 120) : btnXPos, Time.deltaTime * (isOpen ? 7 : 4));
		quickMenuButton.localPosition = postition;

		//Update Rotation of Button
		currentAngleZ = Mathf.LerpAngle(currentAngleZ, isOpen ? (targetAt180 ? 90 : 270) : (targetAt180 ? 180 : 0), Time.deltaTime * (isOpen ? 6 : 6));
		//if (currentAngleZ == Range (90, 1))
		//	targetAt180 = false;
		//else
		//	targetAt180 = true;

		Debug.Log (currentAngleZ);

		quickMenuButton.eulerAngles = new Vector3(0, 0, currentAngleZ);


		//Update Position Y if not there:
		if (onStartAnim_Done == false) {
			QuickMenuButton_OnStart ();
		}
	}

	//Updates Quick Menu Start up Animation.
	void QuickMenuButton_OnStart() {
		Vector3 postition = quickMenuButton.localPosition;
		postition.y = Mathf.Lerp (postition.y, 0, Time.deltaTime * 8);
		quickMenuButton.localPosition = postition;

		if (postition.y == 0) {
			onStartAnim_Done = true;
		}
	}
	
	public void OnClick() {
		isOpen = !isOpen;
	}
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SplashScreen : MonoBehaviour {

	public Animator anim;
	private float lastClickTime = 0f;
	public float catchTime = 0.25f;

	private int OnParameterId;
	const string OnStateName = "On";

	private float startAnimTime = 0f;
	public float animLength = 0.7f;


	void Start() {
		OnParameterId = Animator.StringToHash(OnStateName);
	}

	void Update()
	{
		if (anim.GetBool (OnParameterId) == true)
		if (Time.time - startAnimTime >= animLength)
			anim.SetBool (OnParameterId, false);

		//OnClick()
		if (ClickMode.canClick)
		if (Input.GetMouseButtonUp (0)) {
			if (Time.time - lastClickTime < catchTime) {
				//Double Tap
				SceneManager.LoadScene ("Main");
			} else if (!anim.GetBool (OnParameterId)) {
				//normal click
				startAnimTime = Time.time;
				anim.SetBool (OnParameterId, true);
			}
			lastClickTime = Time.time;
		}
	}
}

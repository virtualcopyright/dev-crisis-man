﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class RPB : MonoBehaviour {
	public GameObject TapGameObject;

	public Transform LoadingBar;
	public Transform TextIndicator;
	public Transform TextLoading;
	[SerializeField] private float currentAmount;
	[SerializeField] private float speed;



	// Update is called once per frame
	void Update () {

		if (Database.database.Progress < 100) {
			//currentAmount += speed * Time.deltaTime;
			TextIndicator.GetComponent<Text> ().text = ((int)Database.database.Progress).ToString() + "%";
			TextLoading.gameObject.SetActive (true);
		} else {
			TextLoading.gameObject.SetActive (false);
			TextIndicator.GetComponent<Text>().text = "DONE!";
			StartCoroutine (WaitForEndOfFrameCoroutine ());

		}
		LoadingBar.GetComponent<Image> ().fillAmount = Database.database.Progress / 100;
	}

	private IEnumerator WaitForEndOfFrameCoroutine () {
		yield return (new WaitForEndOfFrame ());
		TapGameObject.SetActive (true);
		gameObject.SetActive (false);
	}
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class OnClickProfile : MonoBehaviour, IPointerClickHandler {
	public void OnPointerClick(PointerEventData data) {
		foreach (StaffManager.Staff n in StaffManager.profiles) {
			if (gameObject.name == n.Name) {
				GameObject.FindWithTag ("ProfileManager").GetComponent<InitializeProfile> ().OnClick_ViewProfile (n.Index);
				GameObject.FindWithTag ("WindowManager").GetComponent<PanelManager> ().OpenPanel (GameObject.FindWithTag ("ProfileManager").GetComponent<InitializeProfile>().ProfileWindow);

				break;
			}

		}
	}
}
